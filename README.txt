This is a Docker container that allows you to run an old-school Quake 2 server on a modern
operating system. It will accept connections from the original id Software Quake 2 client (version
3.20), as well as modern clients such as Q2PRO.

Part 1: Installation
--------------------

1. Install Docker on your system (if you don't already have it).

2. Copy the file `baseq2/pak0.pak` from your official Quake 2 CD to the directory
`docker-image/pakfiles/baseq2/`.

3. (Optional) Copy the file `xatrix/pak0.pak` from your Quake 2 Mission Pack: The Reckoning CD to
the directory `docker-image/pakfiles/xatrix/`.

4. (Optional) Copy the file `rogue/pak0.pak` from your Quake 2 Mission Pack: Ground Zero CD to the
directory `docker-image/pakfiles/rogue/`.

5. Run the `./build-image.sh` script.


Part 2: Usage
-------------

1. To start your container, open a terminal window and type `./run-container.sh`.

You can optionally pass a parameter to start a different game mode:

    ./run-container.sh dm   (deathmatch, the default)
    ./run-container.sh dm64 (large deathmatch maps for up to 64 players)
    ./run-container.sh ctf  (capture the flag)
    ./run-container.sh mp1  (deathmatch maps from Mission Pack: Ground Zero, requires pak file)
    ./run-container.sh mp2  (deathmatch maps from Mission Pack: The Reckoning, requires pak file)

If your computer has multiple IP addresses (and you only want Quake 2 to listen on one of them),
you can do:

    IP_ADDRESS=123.45.67.89 ./run-container.sh ctf

2. In your Quake 2 client console, connect to your server using the command
`/connect your-servers-hostname`.

3. (Optional) If you want some bots to play with, type `minimumplayers 8` in the terminal window
and hit return. You can also do this from your Quake 2 client by setting an rcon password (defaults
to "password"):

    /rcon_password password
    /minimumplayers 8

4. Play Quake 2!

5. To stop the server, hit ctrl+c in the terminal.


Part 3: Customization
---------------------

In the `./config/` directory you will find a set of .cfg files. The `server.cfg` file is executed
in every game mode. After `server.cfg`, the server will execute the file corresponding to the
chosen game mode (e.g. `dm.cfg`).

There is also a `bots.cfg` file that handles the configuration of the Gladiator bots.

The package that you downloaded contains default versions of each of these files, named
`XXX.cfg.example`. The first time you run the container, these examples will be automatically
copied to actual `XXX.cfg` files that you can modify (or you can do this yourself if you wish to
customize them straight away).
